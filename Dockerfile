FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/hello-standalone.jar hello-standalone.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/hello-standalone.jar"]