package api.custom;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.openapitools.api.ApiResponseMessage;
import org.openapitools.api.GreetingApiService;
import org.openapitools.api.NotFoundException;

public class GreetingsApiServiceImpl extends GreetingApiService {

  @Override
  public Response getGreeting(String subject, SecurityContext securityContext) throws NotFoundException {
    return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "Hello, " + subject)).build();
  }
}